# Quick (and dirty) testing with Vue, TS, etc...
This is a test setup for [Vue](https://vuejs.org/), [Typescript](https://www.typescriptlang.org/) and [Bulma](https://bulma.io/) running on a Node.js [Express](https://expressjs.com/)-server.

## How
1. Install node packages (a shitload of them)
```bash
npm install
```

2. Build it
```
gulp build
```
3. Then just run the server
```
node .
```