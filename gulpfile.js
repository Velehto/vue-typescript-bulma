const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('webpack');
const gulpWebpack = require('gulp-webpack');

const WEBPACK_CONFIG = require('./webpack.config');

gulp.task('ts:build', () => {
    return webpackBuild(false);
});

gulp.task('ts:watch', () => {
    return webpackBuild(true);
})

gulp.task('sass:build', () => {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./static/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./src/sass/**/*.scss', ['sass:build']);
});

gulp.task('vue:copy', () => {
    gulp.src('./node_modules/vue/dist/vue.js')
        .pipe(gulp.dest('./static/3rdParty/vue'));
});

gulp.task('build', ['vue:copy', 'ts:build', 'sass:build']);

gulp.task('default', ['vue:copy', 'ts:watch', 'sass:build', 'sass:watch']);

function webpackBuild(watch) {
    WEBPACK_CONFIG.watch = watch;
    return gulp.src(WEBPACK_CONFIG.entry)
        .pipe(gulpWebpack(WEBPACK_CONFIG, webpack))
        .pipe(gulp.dest('./static/js'));
}