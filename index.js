const express = require('express');
const server = express();

const PORT = 3001;

server.use('/static', express.static('static'));
server.use('/', express.static('public'));

server.listen(PORT, () => {
    console.log('Runnning in port', PORT);
});