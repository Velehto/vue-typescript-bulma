import Vue from "vue";
import Shell from "../vue/shell.vue";

export default class Application {

    private vueVM: Vue;

    constructor() {
        this.vueVM = new Vue({
            el: '#app',
            components: {
                shell: Shell
            }
        });
    }

}